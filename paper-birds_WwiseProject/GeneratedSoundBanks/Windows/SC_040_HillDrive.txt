Event	ID	Name			Wwise Object Path	Notes
	323074791	Sc40_BG			\PaperBirds_01\SC_040_HillDrive\Sc40_BG	
	687238966	Sc_040_HFX_Gondola			\PaperBirds_01\SC_040_HillDrive\Sc_040_HFX_Gondola	
	1414207779	Sc040_Snore_Left			\PaperBirds_01\SC_040_HillDrive\Sc040_Snore_Left	
	3382780494	Sc040_Toto_fs			\PaperBirds_01\SC_040_HillDrive\Sc040_Toto_fs	
	3456431264	Sc40_Rain_On_Lighthouse_Roof			\PaperBirds_01\SC_040_HillDrive\Sc40_Rain_On_Lighthouse_Roof	
	3786600286	Sc040_Snore_Right			\PaperBirds_01\SC_040_HillDrive\Sc040_Snore_Right	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	390779958	Sc_040_HFX_Gondola_st	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\SFX\Sc_040_HFX_Gondola_st_7F79EA08.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_040_HillDrive\Sc_040_HFX_Gondola\Sc_040_HFX_Gondola_st		0
	416995242	040_Toto_fs	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\SFX\040_Toto_fs_26055306.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_040_HillDrive\Foley\040_Toto_fs		0
	461321231	040_Toto_Entrance_Bandoeon_Mvmt	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\SFX\040_Toto_Entrance_Bandoeon_Mvmt_8653E0E2.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_040_HillDrive\Foley\040_Toto_Entrance_Bandoeon_Mvmt		0
	759274105	Sc_040_HFX_Gondola_m	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\SFX\Sc_040_HFX_Gondola_m_F2D3838C.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_040_HillDrive\Sc_040_HFX_Gondola\Sc_040_HFX_Gondola_m		0
	799495858	040_Snore_Left	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\SFX\040_Snore_Left_F188A06A.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_040_HillDrive\Foley\040_Snore_Left		0
	922489679	040_Snore_Right	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\SFX\040_Snore_Right_260AEF3A.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_040_HillDrive\Foley\040_Snore_Right		0
	1019689829	Sc40_Rain_On_Lighthouse_Roof	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\SFX\Sc40_Rain_On_Lighthouse_Roof_CC13B541.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_040_HillDrive\Sc40_Rain_On_Lighthouse_Roof		0

Streamed Audio	ID	Name	Audio source file	Generated audio file	Wwise Object Path	Notes
	546538926	Sc40_BG	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\SFX\Sc40_BG_02b_2B55DB5D.wem	546538926.wem	\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_040_HillDrive\Sc40_BG	


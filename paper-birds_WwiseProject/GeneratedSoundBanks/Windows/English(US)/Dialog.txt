Event	ID	Name			Wwise Object Path	Notes
	184742329	PaperBirds_Dialog_010_02			\PaperBirds_01\Dialog\PaperBirds_Dialog_010_02	
	184742330	PaperBirds_Dialog_010_01			\PaperBirds_01\Dialog\PaperBirds_Dialog_010_01	
	271505248	PaperBirds_DialogTemp			\PaperBirds_01\Dialog\PaperBirds_DialogTemp	
	1437865097	PaperBirds_Dialog_020_01			\PaperBirds_01\Dialog\PaperBirds_Dialog_020_01	
	1437865098	PaperBirds_Dialog_020_02			\PaperBirds_01\Dialog\PaperBirds_Dialog_020_02	
	1675225517	PaperBirds_Dialog_060_01			\PaperBirds_01\Dialog\PaperBirds_Dialog_060_01	
	1675225518	PaperBirds_Dialog_060_02			\PaperBirds_01\Dialog\PaperBirds_Dialog_060_02	
	1675225519	PaperBirds_Dialog_060_03			\PaperBirds_01\Dialog\PaperBirds_Dialog_060_03	
	2928348408	PaperBirds_Dialog_030_01			\PaperBirds_01\Dialog\PaperBirds_Dialog_030_01	
	2928348411	PaperBirds_Dialog_030_02			\PaperBirds_01\Dialog\PaperBirds_Dialog_030_02	
	3094950973	PaperBirds_Dialog_050_02			\PaperBirds_01\Dialog\PaperBirds_Dialog_050_02	
	3607628008	PaperBirds_Dialog_050			\PaperBirds_01\Dialog\PaperBirds_Dialog_050	
	3607628010	PaperBirds_Dialog_052			\PaperBirds_01\Dialog\PaperBirds_Dialog_052	
	3674738449	PaperBirds_Dialog_015			\PaperBirds_01\Dialog\PaperBirds_Dialog_015	

Game Parameter	ID	Name			Wwise Object Path	Notes
	1274160338	MuteDialog			\Default Work Unit\MuteDialog	

Source plug-ins	ID	Name	Type		Wwise Object Path	Notes
	399207264	Wwise Silence	Wwise Silence		\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_DialogTemp_01\Wwise Silence	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	80840345	PaperBirds_Dialog_050_02	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_050_02_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_050_02		0
	208329140	PaperBirds_Dialog_020_02	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_020_02_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_020_02		0
	264429856	PaperBirds_Dialog_015	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_015_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_015		0
	501274610	PaperBirds_Dialog_010_02	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_010_02_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_010_02		0
	523140643	PaperBirds_Dialog_060_02	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_060_02_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_060_02		0
	729155934	PaperBirds_Dialog_030_02	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_030_02_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_030_02		0
	764208871	PaperBirds_Dialog_050	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_050_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_050		0
	825045986	PaperBirds_Dialog_060_03	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_060_03_2_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_060_03		0
	861943476	PaperBirds_Dialog_010_01	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_010_01_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_010_01		0
	971831178	PaperBirds_Dialog_030_01	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_030_01_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_030_01		0
	987791151	PaperBirds_Dialog_020_01	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_020_01_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_020_01		0
	1004111876	PaperBirds_Dialog_060_01	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_060_01_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_060_01		0
	1008601907	PaperBirds_Dialog_052	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_052_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_052		0

Streamed Audio	ID	Name	Audio source file	Generated audio file	Wwise Object Path	Notes
	80840345	PaperBirds_Dialog_050_02	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_050_02_10C4C929.wem	80840345.wem	\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_050_02	
	208329140	PaperBirds_Dialog_020_02	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_020_02_10C4C929.wem	208329140.wem	\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_020_02	
	264429856	PaperBirds_Dialog_015	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_015_10C4C929.wem	264429856.wem	\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_015	
	501274610	PaperBirds_Dialog_010_02	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_010_02_10C4C929.wem	501274610.wem	\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_010_02	
	523140643	PaperBirds_Dialog_060_02	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_060_02_10C4C929.wem	523140643.wem	\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_060_02	
	729155934	PaperBirds_Dialog_030_02	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_030_02_10C4C929.wem	729155934.wem	\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_030_02	
	764208871	PaperBirds_Dialog_050	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_050_10C4C929.wem	764208871.wem	\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_050	
	825045986	PaperBirds_Dialog_060_03	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_060_03_2_10C4C929.wem	825045986.wem	\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_060_03	
	861943476	PaperBirds_Dialog_010_01	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_010_01_10C4C929.wem	861943476.wem	\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_010_01	
	971831178	PaperBirds_Dialog_030_01	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_030_01_10C4C929.wem	971831178.wem	\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_030_01	
	987791151	PaperBirds_Dialog_020_01	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_020_01_10C4C929.wem	987791151.wem	\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_020_01	
	1004111876	PaperBirds_Dialog_060_01	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_060_01_10C4C929.wem	1004111876.wem	\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_060_01	
	1008601907	PaperBirds_Dialog_052	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Windows\Voices\English(US)\PaperBirds_Dialog_052_10C4C929.wem	1008601907.wem	\Actor-Mixer Hierarchy\PaperBirds_01\Dialog\PaperBirds_Dialog_052	


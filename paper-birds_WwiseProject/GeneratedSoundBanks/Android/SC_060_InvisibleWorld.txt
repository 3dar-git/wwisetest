Event	ID	Name			Wwise Object Path	Notes
	49926213	Sc_60_Interaction_SkyAmb			\PaperBirds_01\SC_060_InvisibleWorld\Sc_60_Interaction_SkyAmb	
	1398442474	Sc_60_Interaction_Sparkles_Sun			\PaperBirds_01\SC_060_InvisibleWorld\Sc_60_Interaction_Sparkles_Sun	
	1404778857	Sc_60_wind_transition			\PaperBirds_01\SC_060_InvisibleWorld\Sc_60_wind_transition	
	1423754433	Sc070_Bird_Leaves			\PaperBirds_01\SC_060_InvisibleWorld\Sc070_Bird_Leaves	
	1517010472	Sc70_BG			\PaperBirds_01\SC_060_InvisibleWorld\Sc70_BG	
	2636744408	Sc_60_Interaction_FaceAppear_03			\PaperBirds_01\SC_060_InvisibleWorld\Sc_60_Interaction_FaceAppear_03	
	2636744409	Sc_60_Interaction_FaceAppear_02			\PaperBirds_01\SC_060_InvisibleWorld\Sc_60_Interaction_FaceAppear_02	
	2636744410	Sc_60_Interaction_FaceAppear_01			\PaperBirds_01\SC_060_InvisibleWorld\Sc_60_Interaction_FaceAppear_01	
	2636744415	Sc_60_Interaction_FaceAppear_04			\PaperBirds_01\SC_060_InvisibleWorld\Sc_60_Interaction_FaceAppear_04	
	2979069056	Sc070_EndingSun			\PaperBirds_01\SC_060_InvisibleWorld\Sc070_EndingSun	
	3727780999	Sc_070_Ending_SunReveal			\PaperBirds_01\SC_060_InvisibleWorld\Sc_070_Ending_SunReveal	
	3969358865	Sc_060_Interaction_HandsAppear			\PaperBirds_01\SC_060_InvisibleWorld\Sc_060_Interaction_HandsAppear	
	4009120695	Sc_070_Transition			\PaperBirds_01\SC_060_InvisibleWorld\Sc_070_Transition	
	4132421598	Sc070_Bird_Appear			\PaperBirds_01\SC_060_InvisibleWorld\Sc070_Bird_Appear	
	4252411554	Sc_60_Interaction_Sparkles_R			\PaperBirds_01\SC_060_InvisibleWorld\Sc_60_Interaction_Sparkles_R	
	4252411580	Sc_60_Interaction_Sparkles_L			\PaperBirds_01\SC_060_InvisibleWorld\Sc_60_Interaction_Sparkles_L	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	56755646	Sc_60_Interaction_FaceAppear_01_m	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_60_Interaction_FaceAppear_01_m_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Interaction\Sc_60_Interaction_FaceAppear_01\Sc_60_Interaction_FaceAppear_01_m		0
	402735763	Sc_60_fr21100_wind_transition	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_60_fr21100_wind_transition_02b_CE19D214.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Sc_60_fr21100_wind_transition		0
	416092058	Sc_60_Interaction_FaceAppear_03_m	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_60_Interaction_FaceAppear_03_m_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Interaction\Sc_60_Interaction_FaceAppear_03\Sc_60_Interaction_FaceAppear_03_m		0
	434918296	Sc_60_Interaction_FaceAppear_02_m	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_60_Interaction_FaceAppear_02_m_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Interaction\Sc_60_Interaction_FaceAppear_02\Sc_60_Interaction_FaceAppear_02_m		0
	501256391	Sc_60_Interaction_FaceAppear_03_st	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_60_Interaction_FaceAppear_03_st_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Interaction\Sc_60_Interaction_FaceAppear_03\Sc_60_Interaction_FaceAppear_03_st		0
	592721451	Sc070_Bird_Appear	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc070_Bird_Appear_1B98EF31.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Sc070_Bird_Appear		0
	604066317	Sc_070_Ending_SunReveal	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_070_Ending_SunReveal_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Sc_070_Ending_SunReveal		0
	635806866	Sc_060_HandsAppear	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_010_Bandoneon_HandsAppear_FOA_DAC62D07.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Interaction\Sc_060_HandsAppear		0
	648322664	Sc_60_Interaction_FaceAppear_04_st	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_60_Interaction_FaceAppear_04_st_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Interaction\Sc_60_Interaction_FaceAppear_04\Sc_60_Interaction_FaceAppear_04_st		0
	710339494	Sc_60_Interaction_Sparkles_R	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_60_Interaction_Sparkles_R_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Interaction\Sc_60_Interaction_Sparkles_R		0
	764901110	Sc070_EndingSun	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc070_EndingSun_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Sc070_EndingSun		0
	784706021	Sc_60_Interaction_Sparkles_Sun	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_60_Interaction_Sparkles_Sun_D9AA0EDA.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Interaction\Sc_60_Interaction_Sparkles_Sun		0
	812221425	Sc_60_Interaction_SkyAmb	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_60_Interaction_SkyAmb_FOA_DAC62D07.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Interaction\Sc_60_Interaction_SkyAmb		0
	869189355	Sc_070_Transition	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\070_Transition_2D_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Sc_070_Transition		0
	872852145	Sc_60_Interaction_Sparkles_L	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_60_Interaction_Sparkles_L_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Interaction\Sc_60_Interaction_Sparkles_L		0
	877835185	Sc_60_Interaction_FaceAppear_04_m	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_60_Interaction_FaceAppear_04_m_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Interaction\Sc_60_Interaction_FaceAppear_04\Sc_60_Interaction_FaceAppear_04_m		0
	943543628	Sc070_Bird_Leaves	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc070_Bird_Leaves_186F7029.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Sc070_Bird_Leaves		0
	970612094	Sc_60_Interaction_FaceAppear_02_st	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_60_Interaction_FaceAppear_02_st_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Interaction\Sc_60_Interaction_FaceAppear_02\Sc_60_Interaction_FaceAppear_02_st		0
	1032744243	Sc_60_Interaction_FaceAppear_01_st	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc_60_Interaction_FaceAppear_01_st_10C4C929.wem		\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Interaction\Sc_60_Interaction_FaceAppear_01\Sc_60_Interaction_FaceAppear_01_st		0

Streamed Audio	ID	Name	Audio source file	Generated audio file	Wwise Object Path	Notes
	264157513	Sc70_BG	H:\PaperBird2_WwiseTestTimeline\paper-birds_WwiseProject\.cache\Android\SFX\Sc70_BG_02b_042108C1.wem	264157513.wem	\Actor-Mixer Hierarchy\PaperBirds_01\SFX\SC_060_InvisibleWorld\Sc70_BG	


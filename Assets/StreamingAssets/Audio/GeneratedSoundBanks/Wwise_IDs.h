/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID _2POP = 3133006423U;
        static const AkUniqueID LOGO = 556785088U;
        static const AkUniqueID PAPER_BIRDS_HOME_MENU_PLAY = 2170639425U;
        static const AkUniqueID PAPER_BIRDS_HOME_MENU_STOP = 195631895U;
        static const AkUniqueID PAPERBIRDS_DIALOG_010_01 = 184742330U;
        static const AkUniqueID PAPERBIRDS_DIALOG_010_02 = 184742329U;
        static const AkUniqueID PAPERBIRDS_DIALOG_015 = 3674738449U;
        static const AkUniqueID PAPERBIRDS_DIALOG_020_01 = 1437865097U;
        static const AkUniqueID PAPERBIRDS_DIALOG_020_02 = 1437865098U;
        static const AkUniqueID PAPERBIRDS_DIALOG_030_01 = 2928348408U;
        static const AkUniqueID PAPERBIRDS_DIALOG_030_02 = 2928348411U;
        static const AkUniqueID PAPERBIRDS_DIALOG_050 = 3607628008U;
        static const AkUniqueID PAPERBIRDS_DIALOG_050_02 = 3094950973U;
        static const AkUniqueID PAPERBIRDS_DIALOG_052 = 3607628010U;
        static const AkUniqueID PAPERBIRDS_DIALOG_060_01 = 1675225517U;
        static const AkUniqueID PAPERBIRDS_DIALOG_060_02 = 1675225518U;
        static const AkUniqueID PAPERBIRDS_DIALOG_060_03 = 1675225519U;
        static const AkUniqueID PAPERBIRDS_DIALOGTEMP = 271505248U;
        static const AkUniqueID PAPERBIRDS_INTRO_WHOOSH = 2805371853U;
        static const AkUniqueID PAPERBIRDS_MUSIC_010 = 158596501U;
        static const AkUniqueID PAPERBIRDS_MUSIC_020 = 108263644U;
        static const AkUniqueID PAPERBIRDS_MUSIC_030 = 125041231U;
        static const AkUniqueID PAPERBIRDS_MUSIC_050_SCORE = 1738894402U;
        static const AkUniqueID PAPERBIRDS_MX_030_BANDONEON_SOLO = 1479986466U;
        static const AkUniqueID PAPERBIRDS_MX_040 = 3857974628U;
        static const AkUniqueID PAPERBIRDS_MX_050_CLASSIC = 2807653462U;
        static const AkUniqueID PAPERBIRDS_MX_CREDITS = 55541550U;
        static const AkUniqueID PLANES_BASS_SFX = 4165288442U;
        static const AkUniqueID SC010_BANDONEONSPARKLES = 148962058U;
        static const AkUniqueID SC010_BANDONEONSPARKLES_02 = 3804494475U;
        static const AkUniqueID SC010_BIRD_INTERACTION = 795683329U;
        static const AkUniqueID SC010_SIGN = 2096901194U;
        static const AkUniqueID SC010_SPARKLESEND = 4189360495U;
        static const AkUniqueID SC010_TOTO_FS = 1419193733U;
        static const AkUniqueID SC10_02_BG_WIND_TRANSITION = 463373652U;
        static const AkUniqueID SC10_BG = 1834799902U;
        static const AkUniqueID SC015_AZUL_FS = 311072494U;
        static const AkUniqueID SC015_BALL = 1544584813U;
        static const AkUniqueID SC015_GOALER_FS = 926229168U;
        static const AkUniqueID SC015_ICECREAMMAN = 1401971629U;
        static const AkUniqueID SC015_TOTO_FS = 3203924804U;
        static const AkUniqueID SC15_BG = 1531473661U;
        static const AkUniqueID SC020_DANCING_AZUL_FS_ = 3084502794U;
        static const AkUniqueID SC020_DANCING_DOLL = 3654228534U;
        static const AkUniqueID SC020_TRAVERSAL_AZUL_FS_01 = 4143906709U;
        static const AkUniqueID SC20_01_BG = 2279987765U;
        static const AkUniqueID SC20_02_BG = 541782940U;
        static const AkUniqueID SC20_03_BG = 2060654231U;
        static const AkUniqueID SC20_PENS = 1684046688U;
        static const AkUniqueID SC030_BEDROOM_INVISIBLE_STEADY = 980388018U;
        static const AkUniqueID SC030_BEDROOM_INVISIBLE_TRANSITION = 590331225U;
        static const AkUniqueID SC030_BG_RADIO = 2857145014U;
        static const AkUniqueID SC030_BIRDWINDOW_01 = 2149063172U;
        static const AkUniqueID SC030_BIRDWINDOW_02 = 2149063175U;
        static const AkUniqueID SC030_BIRDWINDOW_03 = 2149063174U;
        static const AkUniqueID SC030_BIRDWINDOW_04 = 2149063169U;
        static const AkUniqueID SC030_BIRDWINDOW_05 = 2149063168U;
        static const AkUniqueID SC030_BIRDWINDOW_06 = 2149063171U;
        static const AkUniqueID SC030_CARAFFE_SPILL = 2018428738U;
        static const AkUniqueID SC030_COOCKIEJAR = 222047923U;
        static const AkUniqueID SC030_FRIDGE = 1504023064U;
        static const AkUniqueID SC030_GLASSTHROW = 43627011U;
        static const AkUniqueID SC030_GRANDMA_DRINKS = 2941310617U;
        static const AkUniqueID SC030_GRANDMA_FS = 2396321889U;
        static const AkUniqueID SC030_GRANDMA_GRABPLATE = 3441548630U;
        static const AkUniqueID SC030_GRANDMA_KISS = 2790994066U;
        static const AkUniqueID SC030_GRANDMA_PICKUP_GLASS = 2050326221U;
        static const AkUniqueID SC030_GRANDMA_ROCKINGCHAIR = 3941664968U;
        static const AkUniqueID SC030_GRANDMA_TEADOWN = 1686793842U;
        static const AkUniqueID SC030_PORTAL_CLOSES = 2890245695U;
        static const AkUniqueID SC030_PORTAL_OPENS = 2796702067U;
        static const AkUniqueID SC030_PORTAL_SUN = 2026127202U;
        static const AkUniqueID SC030_SISTER_APPEAR = 3136282801U;
        static const AkUniqueID SC030_TOTO_BED = 850631715U;
        static const AkUniqueID SC030_TOTO_CARAFFE_PICKUP = 2026660433U;
        static const AkUniqueID SC030_TOTO_EATS = 4160692019U;
        static const AkUniqueID SC030_TOTO_ENTRANCE_PUSHCHAIR = 2219946998U;
        static const AkUniqueID SC030_TOTO_ENTRANCE_SIT = 2955092437U;
        static const AkUniqueID SC030_TOTO_FS = 1518764183U;
        static const AkUniqueID SC030_TOTO_FS_EXIT = 2492117314U;
        static const AkUniqueID SC030_TOTO_GLASS_POUR = 1043524087U;
        static const AkUniqueID SC030_TOTO_HANDSTABLE_GETUP = 93026822U;
        static const AkUniqueID SC030_TOTO_HITSTABLE = 1679389476U;
        static const AkUniqueID SC030_TOTO_ROCKCHAIR = 2645401884U;
        static const AkUniqueID SC30_BG = 2364885260U;
        static const AkUniqueID SC30_BG_WIND_TRANSITION = 547416253U;
        static const AkUniqueID SC040_SNORE_LEFT = 1414207779U;
        static const AkUniqueID SC040_SNORE_RIGHT = 3786600286U;
        static const AkUniqueID SC040_TOTO_FS = 3382780494U;
        static const AkUniqueID SC40_BG = 323074791U;
        static const AkUniqueID SC40_LIGHTNING = 3432955600U;
        static const AkUniqueID SC40_RAIN_ON_LIGHTHOUSE_ROOF = 3456431264U;
        static const AkUniqueID SC050_EARTHQUAKE_ADD = 172499758U;
        static const AkUniqueID SC50_BG_INT = 3246695684U;
        static const AkUniqueID SC52_BG = 2389607484U;
        static const AkUniqueID SC070_BIRD_APPEAR = 4132421598U;
        static const AkUniqueID SC070_BIRD_LEAVES = 1423754433U;
        static const AkUniqueID SC070_ENDINGSUN = 2979069056U;
        static const AkUniqueID SC70_BG = 1517010472U;
        static const AkUniqueID SC_010_DSGN_PAPERBIRDS_INTRO = 1104004298U;
        static const AkUniqueID SC_010_DSGN_PAPERBIRDS_METEOR = 121808876U;
        static const AkUniqueID SC_010_HFX_DOOR = 1771570157U;
        static const AkUniqueID SC_010_HFX_STREETLIGHT_BUZZ = 2483188560U;
        static const AkUniqueID SC_010_HFX_STREETLIGHT_STOPS = 2014017186U;
        static const AkUniqueID SC_010_INTERACTION_BANDONEON_HANDSAPPEAR = 3559372965U;
        static const AkUniqueID SC_010_INTERACTION_BANDONEON_SPIRITS = 3921258286U;
        static const AkUniqueID SC_010_INTERACTION_HANDS_SPARKLE_L = 1430700385U;
        static const AkUniqueID SC_010_INTERACTION_HANDS_SPARKLE_R = 1430700415U;
        static const AkUniqueID SC_010_INTERACTION_MOSALINI_BANDEON = 2903996105U;
        static const AkUniqueID SC_010_INTERACTION_MOSALINI_BANDEON_AMB = 3296304700U;
        static const AkUniqueID SC_010_INTERACTION_MOSALINI_CRISTALES = 3471520034U;
        static const AkUniqueID SC_015_HFX_DOORSCHOOL = 434494226U;
        static const AkUniqueID SC_015_HFX_SOCCERBALL_BOUNCE = 808643463U;
        static const AkUniqueID SC_015_HFX_SOCCERBALL_WATER = 2046356312U;
        static const AkUniqueID SC_015_WALLA_AZUL_01 = 267840413U;
        static const AkUniqueID SC_015_WALLA_TOTO = 225375223U;
        static const AkUniqueID SC_020_AZUL_DISAPPEARS = 1797868890U;
        static const AkUniqueID SC_020_AZUL_DISAPPEARS_SHADOW = 3414069025U;
        static const AkUniqueID SC_020_AZUL_DISAPPEARS_WIND = 3221508055U;
        static const AkUniqueID SC_020_PAPERBIRD_LANDS_GO = 2747083490U;
        static const AkUniqueID SC_020_WALLA_AZUL_02 = 3456548274U;
        static const AkUniqueID SC_020_WALLA_AZUL_03 = 3456548275U;
        static const AkUniqueID SC_030_GRANDMA_DRINKS_ENTRANCE = 455643817U;
        static const AkUniqueID SC_030_GRANDMA_PIPEIN = 739257072U;
        static const AkUniqueID SC_030_GRANDMA_PIPEOUT = 880274903U;
        static const AkUniqueID SC_030_PAPERBIRD_SKYGLOW = 4077967596U;
        static const AkUniqueID SC_030_PAPERBIRDSKY = 2036386020U;
        static const AkUniqueID SC_030_PORTAL_HANDSAPPEAR = 619444792U;
        static const AkUniqueID SC_030_TOTO_FS_BIRD = 2428968998U;
        static const AkUniqueID SC_030_WALLA_ABUELA = 4184070260U;
        static const AkUniqueID SC_030_WALLA_TOTO = 950693110U;
        static const AkUniqueID SC_040_HFX_GONDOLA = 687238966U;
        static const AkUniqueID SC_050_052_TRANSITION_MAGIC = 1988879589U;
        static const AkUniqueID SC_050_BOOK = 3613098015U;
        static const AkUniqueID SC_050_DEBRIS_1STFRAME = 868346907U;
        static const AkUniqueID SC_050_DEBRIS_2NDFRAME = 3558000467U;
        static const AkUniqueID SC_050_DEBRIS_FLOWER = 35622621U;
        static const AkUniqueID SC_050_DEBRIS_LAMP = 123466942U;
        static const AkUniqueID SC_050_DEBRIS_SHAKES_CUPBOARD = 2529411204U;
        static const AkUniqueID SC_050_DEBRIS_SHAKES_FRAMES = 1750749686U;
        static const AkUniqueID SC_050_DEBRIS_SHAKES_LAMP = 1579142542U;
        static const AkUniqueID SC_050_DEBRIS_SHAKES_WOODPILE = 438866807U;
        static const AkUniqueID SC_050_DEBRIS_WOODPILE = 834488967U;
        static const AkUniqueID SC_050_GLASSES = 1298650976U;
        static const AkUniqueID SC_050_GRANDPA_BENCHSIT = 743919028U;
        static const AkUniqueID SC_050_GRANDPA_BENCHUP = 746788415U;
        static const AkUniqueID SC_050_GRANDPA_CHAIR_01 = 3076535805U;
        static const AkUniqueID SC_050_GRANDPA_CHAIR_02 = 3076535806U;
        static const AkUniqueID SC_050_GRANDPA_CHAIR_HAND_01 = 955573051U;
        static const AkUniqueID SC_050_GRANDPA_DOOR = 2580148754U;
        static const AkUniqueID SC_050_GRANDPA_DRAWERS = 4207011072U;
        static const AkUniqueID SC_050_GRANDPA_FS_01 = 1682878763U;
        static const AkUniqueID SC_050_GRANDPA_FS_02 = 1682878760U;
        static const AkUniqueID SC_050_GRANDPA_HAT = 1376331873U;
        static const AkUniqueID SC_050_GRANDPA_HUG = 1040779518U;
        static const AkUniqueID SC_050_GRANDPA_KEYS = 529014082U;
        static const AkUniqueID SC_050_HFX_DOORBELL = 997829478U;
        static const AkUniqueID SC_050_HFX_DOORTRAP = 2407809980U;
        static const AkUniqueID SC_050_LIGHTSHINE_STAIRWAY = 2670127300U;
        static const AkUniqueID SC_050_PAPERBIRD_OUTOFPOCKET_GLOW = 3840233589U;
        static const AkUniqueID SC_050_PAPERBIRD_OUTOFPOCKET_WINGS = 3958470570U;
        static const AkUniqueID SC_050_PAPERBIRD_TO_DOOR = 4143685956U;
        static const AkUniqueID SC_050_PAPERBIRD_TO_DOOR_GLOW = 2634400496U;
        static const AkUniqueID SC_050_PAPEROUT = 1721121450U;
        static const AkUniqueID SC_050_PENPAPER = 858089757U;
        static const AkUniqueID SC_050_TOTO_FS = 4765490U;
        static const AkUniqueID SC_050_TYPEWRITER = 1483615501U;
        static const AkUniqueID SC_050_TYPEWRITER_PAPEROUT = 1368592234U;
        static const AkUniqueID SC_050_WALLA_GRANDPA_01 = 691335503U;
        static const AkUniqueID SC_050_WALLA_GRANDPA_02 = 691335500U;
        static const AkUniqueID SC_050_WALLA_TOTO = 383962896U;
        static const AkUniqueID SC_052_CAGE_FLYBY_01 = 2880609979U;
        static const AkUniqueID SC_052_CAGE_FLYBY_02 = 2880609976U;
        static const AkUniqueID SC_052_CAGE_LEFT = 2308192886U;
        static const AkUniqueID SC_052_CAGE_RIGHT = 1043678325U;
        static const AkUniqueID SC_052_HFX_CAGE_DOOR = 269214154U;
        static const AkUniqueID SC_052_HFX_CAGE_FLYBY_01 = 3470183744U;
        static const AkUniqueID SC_052_HFX_CAGE_FLYBY_02 = 3470183747U;
        static const AkUniqueID SC_052_HFX_CAGE_FLYBY_03 = 3470183746U;
        static const AkUniqueID SC_052_HFX_CAGE_RATTLE_01 = 715907038U;
        static const AkUniqueID SC_052_LIGHTSHINE_CAGES = 1064737557U;
        static const AkUniqueID SC_052_PAPERBIRD_DISAPPEARS = 1197946578U;
        static const AkUniqueID SC_052_PAPERBIRD_FLYTODISAPPEAR_M = 1613903927U;
        static const AkUniqueID SC_052_PAPERBIRD_OUTOFCAGE_WINGS = 1733224962U;
        static const AkUniqueID SC_052_TOTO_FS = 3585613796U;
        static const AkUniqueID SC_052_VORTEX = 2546574370U;
        static const AkUniqueID SC_052_VORTEX_TRANS = 2322509469U;
        static const AkUniqueID SC_060_INTERACTION_HANDSAPPEAR = 3969358865U;
        static const AkUniqueID SC_60_INTERACTION_FACEAPPEAR_01 = 2636744410U;
        static const AkUniqueID SC_60_INTERACTION_FACEAPPEAR_02 = 2636744409U;
        static const AkUniqueID SC_60_INTERACTION_FACEAPPEAR_03 = 2636744408U;
        static const AkUniqueID SC_60_INTERACTION_FACEAPPEAR_04 = 2636744415U;
        static const AkUniqueID SC_60_INTERACTION_SKYAMB = 49926213U;
        static const AkUniqueID SC_60_INTERACTION_SPARKLES_L = 4252411580U;
        static const AkUniqueID SC_60_INTERACTION_SPARKLES_R = 4252411554U;
        static const AkUniqueID SC_60_INTERACTION_SPARKLES_SUN = 1398442474U;
        static const AkUniqueID SC_60_WIND_TRANSITION = 1404778857U;
        static const AkUniqueID SC_070_ENDING_SUNREVEAL = 3727780999U;
        static const AkUniqueID SC_070_TRANSITION = 4009120695U;
        static const AkUniqueID SC_160_MUS_INTERACTIVE_PART_01_A = 4014710930U;
        static const AkUniqueID STEMS_LEFT_HAND = 2294283733U;
        static const AkUniqueID STEMS_RIGHT_HAND = 1343774394U;
    } // namespace EVENTS

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID BANDONEONVOLUME = 2602389325U;
        static const AkUniqueID CRYSTALVOLUME = 573230919U;
        static const AkUniqueID CUERDAS_TRIGGER = 3432348709U;
        static const AkUniqueID HANDVELOCITY = 2601967367U;
        static const AkUniqueID MUTEDIALOG = 1274160338U;
        static const AkUniqueID PLANES_HEAD_DISTANCE = 3672599489U;
        static const AkUniqueID PORTALSCALE = 1758294411U;
        static const AkUniqueID PORTALTOSUNANGLE = 2155438739U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID DIALOG = 1235749641U;
        static const AkUniqueID EP2_DIALOG = 1211344803U;
        static const AkUniqueID EP2_GLOBAL = 273309434U;
        static const AkUniqueID EP2_MUSIC = 2433942396U;
        static const AkUniqueID GLOBAL = 1465331116U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SC_010_TOWNSCHOOL = 1094905252U;
        static const AkUniqueID SC_015_SCHOOLTRANSITION = 1645756342U;
        static const AkUniqueID SC_020_LAKE = 662944586U;
        static const AkUniqueID SC_030_TOTOHOUSE = 1910729374U;
        static const AkUniqueID SC_040_HILLDRIVE = 3655866056U;
        static const AkUniqueID SC_050_GRANDFATHERHOUSE = 358864552U;
        static const AkUniqueID SC_052_GRANDFATHERBASEMENT = 2724845993U;
        static const AkUniqueID SC_060_INVISIBLEWORLD = 2228011512U;
        static const AkUniqueID SC_080_JAZZCLUB = 2897780134U;
        static const AkUniqueID SC_090_CRIME = 171237782U;
        static const AkUniqueID SC_100_TOTOSHADOW = 3632458682U;
        static const AkUniqueID SC_130_GRANDMATOWN = 205268403U;
        static const AkUniqueID SC_140_GRANDMACAVE = 3417731117U;
        static const AkUniqueID SC_150_AERIALFOREST = 3064240050U;
        static const AkUniqueID SC_160_ORCHESTRA = 121071825U;
        static const AkUniqueID SC_170_EPILOGUE = 3401060741U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENCES = 1017660616U;
        static const AkUniqueID DIALOG = 1235749641U;
        static const AkUniqueID ENV_BUS = 3962846285U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC_2D = 1939884427U;
        static const AkUniqueID MUSIC_AMB = 2815327375U;
        static const AkUniqueID NARRATOR = 3272864290U;
        static const AkUniqueID NON_ENV_BUS = 1418599241U;
        static const AkUniqueID REVERBS = 3545700988U;
        static const AkUniqueID SFX = 393239870U;
        static const AkUniqueID SFX_2D = 2839302323U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID CAVE = 4122393694U;
        static const AkUniqueID HOUSE = 3720479411U;
        static const AkUniqueID NARR = 948652494U;
        static const AkUniqueID SCHOOL = 3661816265U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__

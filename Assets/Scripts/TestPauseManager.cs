﻿using System;
using UnityEngine;
using UnityEngine.Playables;

public class TestPauseManager : MonoBehaviour
{
    [SerializeField] PlayableDirector director;
    [SerializeField] double timeToAdvence = 1;

	bool isPaused;

	void Update ()
	{
        if (Input.GetKeyDown (KeyCode.Space))
            PauseResume ();

        if (Input.GetKeyDown (KeyCode.RightArrow))
            AdvanceTimeline (timeToAdvence);

        if (Input.GetKeyDown (KeyCode.LeftArrow))
            AdvanceTimeline (-timeToAdvence);
    }

	void PauseResume ()
    {
        if (isPaused)
            ApplicationResume ();
        else
            ApplicationPause ();
    }

	void ApplicationPause ()
    {
        if (isPaused)
            return;
        
        isPaused = true;
        Time.timeScale = 0;
        AkSoundEngine.Suspend (false);
        AkSoundEngine.RenderAudio ();
    }

    void ApplicationResume ()
    {
        if (!isPaused)
            return;
        
        isPaused = false;
        Time.timeScale = 1;
        AkSoundEngine.WakeupFromSuspend ();
        AkSoundEngine.RenderAudio ();
    }

	private void AdvanceTimeline (double time)
	{
        if (director)
            director.time += time;
	}
}